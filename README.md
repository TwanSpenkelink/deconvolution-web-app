# README #

### DeconCell web-app ###

* Set-up
* Known bugs
* Contact

### How do I get set up? ###

Run the app.R script to launch the web-app. Use the web-app with any browser. The following packages are needed:

* shiny
* shinyjs
* deconCell (https://github.com/raguirreg/DeconCell)
* edgeR

### Known bugs ###

* Loading spinner sometimes disappears at second run
* Height of download button not fixed (FIXED)
* Location of download button not saved, results in shift of elements

### Who do I talk to? ###

* Twan Spenkelink | t.o.spenkelink@umcg.nl
